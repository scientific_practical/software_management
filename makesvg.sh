#!/bin/bash
files=$1
pdflatex $files
pdfcrop ${files}.pdf ${files}_crop.pdf
inkscape --without-gui --file=${files}_crop.pdf -T --export-plain-svg=${files}.svg
rm ${files}_crop.pdf ${files}.pdf 
rm ${files}.aux ${files}.log
